-- load a generated world in legends mode - intended to be run on startup
--[====[

load-legends-mode
=========

When run on the title screen, loads the world with the
given folder name without requiring interaction.

Example: ``load-legends-mode region5``

This can also be run when starting DFHack from the command line::

    ./dfhack +load-legends-mode region1

Based on load-save.lua

]====]

local gui = require 'gui'
local script = require 'gui.script'

-- The name of the folder or region, Example: `region10`
local folder_name = ({...})[1] or qerror("No folder name given")
-- Maximum amount of timer to start
local max_loops = ({...})[2] or 20

-- This should be execuded when on the main title screen
-- Select "Start Playing"
local title_screen = dfhack.gui.getViewscreenByType(df.viewscreen_titlest, 0)
if not title_screen then
    qerror("Can't find title game screen")
end
local found = false
for idx, item in ipairs(title_screen.menu_line_id) do
    if item == df.viewscreen_titlest.T_menu_line_id.Start then
        found = true
        title_screen.sel_menu_line = idx
        break
    end
end
if not found then
    qerror("Can't find 'Start Playing' option")
end
gui.simulateInput(title_screen, 'SELECT')

-- The `Start Playing` screen is still part of the title screen.
startplaying_screen = dfhack.gui.getViewscreenByType(df.viewscreen_titlest, 0) or
    qerror("Can't find start playing screen")

-- Select correct region
-- This uses the `folder_name` to select the right region
local found = false
for idx, save in ipairs(startplaying_screen.start_savegames) do
    if save.save_dir == folder_name then
        found = true
        startplaying_screen.sel_submenu_line = idx
        break
    end
end
if not found then
    qerror("Can't find save: " .. folder_name)
end
-- Select the region
gui.simulateInput(startplaying_screen, 'SELECT')

-- Select "Legends"
local found = false
for idx, submenu in ipairs(startplaying_screen.submenu_line_text) do
    if submenu.value == "Legends" then
        found = true
        startplaying_screen.sel_menu_line = idx
        break
    end
end
if not found then
    qerror("Can't find legends mode")
end
-- Select the option "Legends"
gui.simulateInput(title_screen, 'SELECT')

-- Wait for the the loading of the legends and print message
-- The function is recursively called in order to wait for the legends to be loaded.
function waitLoading(max_loops)
    local current_screen = dfhack.gui.getCurViewscreen()
    --print("check1: " .. tostring(df.viewscreen_legendsst:is_instance(current_screen)))
    --if df.viewscreen_legendsst:is_instance(current_screen) then
    --  print("check2: " .. tostring(current_screen.init_step == -1))
    --end
    if df.viewscreen_legendsst:is_instance(current_screen) and 
      current_screen.init_step == -1 then
        -- This message is used to detect if the loading is finished by external scripts
        -- NOTE: Please to not change this message!
        print("Done loading legends!")
    else
        if max_loops-1 >= 0 then
            --print("Timer starting: "..tostring(max_loops-1))
            -- Start a new timer
            dfhack.timeout(120, 'frames', function() waitLoading(max_loops-1) end)
        else
            -- If maximum amount of timers reached
            -- NOTE: Please to not change this message!
            print("Done loading legends!")
        end
    end
end

-- Wait for the legends to be loaded. Once done print "Done loading legends!"
dfhack.timeout(120, 'frames', function() waitLoading(max_loops) end)


