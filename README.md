# DF World Gen
This script will generate multiple worlds in [Dwarf Fortress](https://www.bay12games.com/dwarves/).
To execute this script [DFHack](https://github.com/DFHack/dfhack) is required!

This script only works in Linux (and maybe Mac OSX).

If you are using Windows take a look here: 
[https://github.com/Nikorasu/DwarfGenManager](https://github.com/Nikorasu/DwarfGenManager)

## Install
Make sure Dwarf Fortress and DFHack are installed.
Go to the folder where the `df` and `dfhack` executables are.
Place the `worldgen.sh` file in this folder.

Place the `load-legends-mode.lua` in the folder `./hack/scripts/`.

Make sure you have created the `dfhack.init` file, renaming the `dfhack.init-example` will get you started. 
(This will prevent a message from popping up at startup.)

We also recommend changing the DF `data/init/init.txt` file:
```
Turn the sound of (unless you want the music while generating)
[SOUND:NO]
Turn of the into this makes generation and exporting faster.
[INTRO:NO]
```

## Running the script
Consider opening the file in a text editor to check if everything is set up correctly.
The size of the world can be configured in the file.

Once everything is installed you can run the `worldgen.sh` in a terminal.
Make sure you terminal is op in the same folder as where you installed it.

Format:
```bash
./worldgen.sh AMOUNT_OF_WORLDS STARTNUMBER
```

Example:
```bash
# Generate world 50, 51 and 52
./worldgen.sh 3 50
```

NOTE: The world should not already exist! 

If the world already exist the generation will fail (not in a nice way) 
and it will just reexport all the legends.
