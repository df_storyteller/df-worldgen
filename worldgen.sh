#!/bin/bash
# Generate mutiple worlds in Dwarf Fortress and export all there legends.
#
# Format:
#  $ ./worldgen.sh AMOUNT_OF_WORLDS STARTNUMBER
# Example:
#  $ ./worldgen.sh 3 50
# This will generate world 50, 51 and 52
# NOTE: The world should not already exist!

AMOUNT=$1
STARTID=$2

########## Settings ##########

#SIZE="LARGE"
#SIZE="MEDIUM"
#SIZE="SMALL"
#SIZE="SMALLER"
SIZE="POCKET"

#WORLD_TYPE="ISLAND"
WORLD_TYPE="REGION"

# This allows DFHack to not display to much noise into the output file.
export DFHACK_DISABLE_CONSOLE=1
# This disables the interface of Dwarf Fortress
#export DFHACK_HEADLESS=1

# A temporary file used to store the output of DFHack.
# This file should be in the `/tmp/` folder.
TEMP_OUPUT=/tmp/df_worldgen_output.txt
# Create the temporary file
# The .XXXXXXX will be replace with some random alpha-numeric characters
# This makes sure it can always create this file.
tmpfile=$(mktemp "$TEMP_OUPUT.XXXXXXX")

########## Code ##########

# Wait for the exporting of the legeneds to finish
# It does this by waiting for a message in the `tmpfile` file
wait_for_export_to_finish () {
  echo "Waiting..."
  # search for the string in the output, if found, stop waiting.
  tail -f -n1 ${tmpfile} | grep -qe "Done exporting."
  if [ $? == 0 ]; then
      echo "Done exporting"
  fi
}

# Wait for the loading of the legeneds to finish
# It does this by waiting for a message in the `tmpfile` file
wait_for_loading_to_finish () {
  echo "Waiting..."
  # search for the string in the output, if found, stop waiting.
  tail -f -n1 ${tmpfile} | grep -qe "Done loading legends!"
  if [ $? == 0 ]; then
      echo "Done loading"
  fi
}

# The main loop to generate all the worlds
for (( i=0; i<$AMOUNT; i++ ))
do
  # clean tempfile
  echo "" > "$tmpfile"
  # Start
  WORLDID=$(( $STARTID + $i ))
  echo "Generating World: $WORLDID"
  # Generate the world and close Dwarf Fortress when done.
  ./dfhack "df -gen $WORLDID RANDOM \"$SIZE $WORLD_TYPE\""
  # Just to be kind to your pc
  sleep 1
  echo "Exporting legends"
  # Open DFHack and load the recently generated world in legends mode.
  script -q -a -f -c "./dfhack +load-legends-mode region$WORLDID 20" "$tmpfile" &
  # Wait for legends to be loaded
  wait_for_loading_to_finish

  # Wait a bit to make sure everything is done
  sleep 1
  echo "Exporting"
  # Export the legends
  ./dfhack-run exportlegends all
  # Wait for the legends to finish exporting
  wait_for_export_to_finish

  # Wait a bit to make sure everything is done
  sleep 3
  echo "Contining by closing down"
  # Close DFHack and Dwarf Fortress
  ./dfhack-run die
done

# Remove the tempfile
rm "$tmpfile"
